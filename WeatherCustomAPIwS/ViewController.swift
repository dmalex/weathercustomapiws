//
// ViewController.swift
// WeatherCustomAPIwS
// Created by Dmitry Alexandrov
// B.RF Group
// Read it:
// https://ioscoachfrank.com/remove-main-storyboard.html
// https://www.weather-forecast.com/locations/moscow/forecasts/latest
// http://localhost:8080/city/moscow
//
import UIKit

class ViewController: UIViewController, UITextFieldDelegate
{
    var topView: UIView?
    var topLabel: UILabel?
    var inputCity: UITextField?
    var getPredictButton: UIButton?
    var predictTextView: UITextView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        addView()
        inputCity!.delegate = self
    }
    
    private func setupView() {
        topLabel = {
            let control = UILabel()
            control.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            control.textAlignment = .center
            control.textColor = UIColor.lightGray
            control.text = "Weather forecast"
            control.translatesAutoresizingMaskIntoConstraints = false // required
            return control
        }()
        
        inputCity = {
            let control = UITextField()
            control.font = UIFont.systemFont(ofSize: 16, weight: .bold)
            control.textAlignment = .center
            control.textColor = UIColor.black
            control.backgroundColor = UIColor.white
            control.placeholder = "Enter city"
            control.textAlignment = .center
            control.translatesAutoresizingMaskIntoConstraints = false // required
            control.layer.cornerRadius = 4
            control.clipsToBounds = false
            return control
        }()
        
        getPredictButton = {
            let control = UIButton()
            control.layer.cornerRadius = 8
            control.clipsToBounds = false
            control.translatesAutoresizingMaskIntoConstraints = false // required
            control.setTitle("Get forecast", for: .normal)
            control.addTarget(self, action: #selector(getForecast), for: .touchUpInside)
            control.addTarget(self, action: #selector(holdDown), for: .touchDown)
            control.isUserInteractionEnabled = false
            control.backgroundColor = UIColor.darkGray
            control.tag = 1
            return control
        }()
        
        predictTextView = {
            let control = UITextView()
            control.backgroundColor = UIColor.black
            control.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            control.textAlignment = .center
            control.textColor = UIColor.systemGreen
            control.text = "Enter city and tap 'Get forecast'."
            control.translatesAutoresizingMaskIntoConstraints = false // required
            control.layer.cornerRadius = 4
            control.clipsToBounds = false
            control.isEditable = false
            return control
        }()
    }
    
    
    func addView() {
        view.addSubview(topLabel!)
        view.addSubview(getPredictButton!)
        view.addSubview(inputCity!)
        view.addSubview(predictTextView!)
        
        topLabel!.topAnchor.constraint(equalTo: view.topAnchor, constant: 70).isActive =  true
        topLabel!.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive =  true
        
        inputCity!.topAnchor.constraint(equalTo: topLabel!.bottomAnchor, constant: 20).isActive = true
        inputCity!.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive =  true
        inputCity!.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -20).isActive = true
        inputCity!.heightAnchor.constraint(equalToConstant: 36).isActive = true
        
        getPredictButton!.topAnchor.constraint(equalTo: inputCity!.bottomAnchor, constant: 20).isActive = true
        getPredictButton!.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        getPredictButton!.heightAnchor.constraint(equalToConstant: 36).isActive = true
        getPredictButton!.widthAnchor.constraint(equalToConstant: 180).isActive = true
        
        predictTextView!.topAnchor.constraint(equalTo: getPredictButton!.bottomAnchor, constant: 20).isActive = true
        predictTextView!.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        predictTextView!.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        predictTextView!.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
    }
    
    
    // target functions
    @objc func holdDown(_ sender: UIButton)
    {
        getPredictButton?.backgroundColor = UIColor.systemBlue
    }
    
    
    @objc func getForecast(_ sender: UIButton) {
        var s = inputCity!.text!.trimmingTrailingSpaces()
        
        getPredictButton?.backgroundColor = UIColor.systemGreen
        
        s = s.trimmingLeadingSpaces()
        inputCity!.text = s
        if s == "" {
            predictTextView!.text = "Enter city and tap 'Get forecast'."
            return
        }
        var weather = ""
        
        let url = URL(string: "http://localhost:8080/city/\(s.replacingOccurrences(of: " ", with: "-"))")
        
        if url != nil {
            var request = URLRequest(url: url!)
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "GET"

            let unreserved = "-._~/?"
            let allowed = NSMutableCharacterSet.alphanumeric()
            allowed.addCharacters(in: unreserved)

            // request.httpBody = body.data(using: String.Encoding.utf8)
            
            var urlError = false
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                if error == nil {
                    print(url!)
                    let jsonWeatherCity = (try! JSONSerialization.jsonObject(with: data!, options: [])) as AnyObject
                    print(jsonWeatherCity)
                    
                    let decoder = JSONDecoder()
                    do {
                        let city = try decoder.decode(WeatherCity.self, from: data!)
                        print(city)
                        weather += String(city.data)
                    } catch {
                        print("Error in JSON parcing.")
                        do {
                            let message = try decoder.decode(ErrorMessage.self, from: data!)
                            print(message)
                            weather += String(message.message)
                        } catch {
                            print("Error in JSON parcing.")
                            weather += "Something went wrong."
                        }
                    }
                    print("Data:", data!)
                } else {
                    urlError = true
                    print("Error occured.")
                    weather += "Something went wrong."
                }
                
                DispatchQueue.main.async {
                    if urlError == true {
                        self.showError(s)
                    } else {
                        self.predictTextView!.text = weather
                    }
                }
            }
            task.resume()
        } else {
            showError(s)
        }
    }
    
    
    func showError(_ s: String) {
        predictTextView!.text = "Was not able to find weather forecast for \(s). Please try again."
    }
    
    
    // Resigns the keyboard if the user presses anywhere on the screen
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // resigns the keyboard when user presses the return/next key on keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        inputCity!.resignFirstResponder()
        getForecast(getPredictButton!)
        checkButton()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        checkButton()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        checkButton()
    }
    
    func checkButton() {
        if (inputCity?.text!.count)! == 0 {
            getPredictButton?.isUserInteractionEnabled = false
            getPredictButton?.backgroundColor = UIColor.darkGray
            predictTextView!.text = "Enter city and tap 'Get forecast'."
        } else {
            getPredictButton?.isUserInteractionEnabled = true
            getPredictButton?.backgroundColor = UIColor.systemGreen
        }
    }
}


struct WeatherCity: Codable {
    var data : String
}

struct ErrorMessage: Codable {
    var message : String
}


extension String {
    func trimmingTrailingSpaces() -> String {
        var s = self
        while s.hasSuffix(" ") {
            s = "" + s.dropLast()
        }
        return s
    }
    
    func trimmingLeadingSpaces() -> String {
        var s = self
        while s.hasPrefix(" ") {
            s = s.dropFirst() + ""
        }
        return s
    }
}
